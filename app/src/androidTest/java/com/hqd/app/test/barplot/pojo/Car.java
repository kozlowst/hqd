package com.hqd.app.test.barplot.pojo;

import com.hqd.app.barplot.crom.annotation.Column;
import com.hqd.app.barplot.crom.annotation.Table;
import com.hqd.app.barplot.crom.Types;

/**
 * Created by tomek on 4/19/14.
 */
@Table
public class Car {

    @Column(type = Types.COLUMN_TYPE_STRING)
    private String make;

    @Column(type = Types.COLUMN_TYPE_STRING)
    private String model;

    @Column(type = Types.COLUMN_TYPE_INTEGER)
    private Integer year;

    @Column(type = Types.COLUMN_TYPE_INTEGER)
    private Integer milage;

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public Integer getYear() {
        return year;
    }

    public Integer getMilage() {
        return milage;
    }
}
