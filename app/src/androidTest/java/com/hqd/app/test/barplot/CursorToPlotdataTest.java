package com.hqd.app.test.barplot;

import android.database.Cursor;

import com.hqd.app.barplot.CursorDataConverter;
import com.hqd.app.barplot.PlotData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static com.hqd.app.test.barplot.utils.CursorBuilder.*;

/**
 * Created by tomek on 4/16/14.
 */

@RunWith(RobolectricTestRunner.class)
public class CursorToPlotdataTest {

    @Test
    public void testCursorGenerator() {
        Cursor cursor = createBasic();
        assertEquals(6, cursor.getCount());
        cursor.moveToFirst();
        assertEquals(NAME1, cursor.getString(1));
    }

    @Test
    public void testSimpleData() {
        Cursor cursor = createBasic();
        PlotData plotData = CursorDataConverter.createPlotData(cursor, 1);
        assertThat(plotData.getX())
                .hasSize(7)
                .contains("", NAME1, NAME2, NAME3, NAME4, NAME5, NAME6);
        assertThat(plotData.getY())
                .hasSize(7)
                .contains(0,1);
    }

    @Test
    public void testTableNames() {
        PlotData plotData = CursorDataConverter.createPlotData(createTableNames(), 1);
        assertThat(plotData.getX())
                .hasSize(5)
                .contains("", NAME1, NAME2, NAME3, NAME4);
        assertThat(plotData.getY())
                .contains(0, 0)
                .contains(1, 1)
                .contains(2, 2)
                .contains(3, 3)
                .contains(4, 4);
    }

}
