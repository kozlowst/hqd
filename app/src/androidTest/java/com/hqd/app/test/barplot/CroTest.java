package com.hqd.app.test.barplot;

import android.database.Cursor;

import com.google.common.collect.Lists;
import com.hqd.app.barplot.Employee;
import com.hqd.app.barplot.crom.Cro;
import com.hqd.app.test.barplot.pojo.Car;
import com.hqd.app.test.barplot.utils.CursorBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by tomek on 4/19/14.
 */
@RunWith(RobolectricTestRunner.class)
public class CroTest {

    @Test
    public void testEmployeeTable() {

        List<Object[]> rows = Lists.newArrayList();
        rows.add(new Object[]{1, "Tomek", 12});

        Cursor cursor = CursorBuilder.create(new String[]{"_id", "name", "salaries"}, rows);
        cursor.moveToFirst();
        Cro cro = new Cro(Employee.class);
        Employee employee = (Employee) cro.populate(cursor);
        System.out.println(employee.getName() + ", " + employee.getSalary());
        assertEquals("Tomek", employee.getName());
        assertEquals(12, employee.getSalary().intValue());
    }

    @Test
    public void testCarTable() {

        Cursor cursor = CursorBuilder.create(
                new String[] {"make", "model", "year", "milage"},
                new Object[] {"Toyota", "Yaris", 2000, 150000});
        cursor.moveToFirst();
        Cro cro = new Cro(Car.class);
        Car car = (Car) cro.populate(cursor);
        assertEquals("Toyota", car.getMake());
        assertEquals("Yaris", car.getModel());
        assertEquals(2000, car.getYear().intValue());
        assertEquals(150000, car.getMilage().intValue());

    }

}
