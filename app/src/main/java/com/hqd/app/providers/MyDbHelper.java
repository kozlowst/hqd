package com.hqd.app.providers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by tomek on 4/5/14.
 */
public class MyDbHelper extends SQLiteOpenHelper {

    public MyDbHelper(Context context) {
        super(context, DbSchema.DB_NAME, null, DbSchema.VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DbSchema.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVer, int newVer) {
        Log.d("TAG", "Check if DB upgrade is needed");
        if (oldVer < 2) {
            Log.d("TAG", "Drop V1");
            sqLiteDatabase.execSQL(DbSchema.DROP_TABLE_V1);
        }
        onCreate(sqLiteDatabase);
    }
}
