package com.hqd.app.providers;

import android.annotation.TargetApi;
import android.content.*;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import com.hqd.app.R;

public class EmployeesActivity extends ActionBarActivity {

    private void showEmployeesList() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new ShowAllEmployeesFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new AddEmployeFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.employees, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class AddEmployeFragment extends Fragment implements View.OnClickListener {

        private EditText name;
        private EditText salary;

        public AddEmployeFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main2, container, false);
            name = (EditText) rootView.findViewById(R.id.employeName);
            salary = (EditText) rootView.findViewById(R.id.employeSalary);
            rootView.findViewById(R.id.addButton).setOnClickListener(this);
            rootView.findViewById(R.id.showAllButton).setOnClickListener(this);
            return rootView;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.addButton:
                    Log.d("TAG", "Add button clicked");
                    Intent intent = new Intent(getActivity(), SalalryIntentService.class);
                    intent.putExtra(MyContract.Employees.NAME, name.getText().toString());
                    intent.putExtra(MyContract.Employees.AMOUNT, salary.getText().toString());
                    intent.setAction(SalalryIntentService.ACTION_INSERT);
                    getActivity().startService(intent);
                    break;
                case R.id.showAllButton:
                    Log.d("TAG", "Show all clicked");
                    ((EmployeesActivity)getActivity()).showEmployeesList();
                    break;
            }

        }
    }

    public static class ShowAllEmployeesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                    getActivity(),
                    android.R.layout.simple_list_item_2,
                    null,
                    new String[]{MyContract.Employees.NAME, MyContract.Employees.AMOUNT},
                    new int[]{android.R.id.text1, android.R.id.text2}, 0);

            this.setListAdapter(cursorAdapter);
            getLoaderManager().initLoader(123, null, this);
        }


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(
                    getActivity(),
                    MyContract.Employees.CONTENT_URI,
                    MyContract.Employees.PROJECTION_ALL,
                    null,
                    null,
                    MyContract.Employees.SORT_ORDER_DEFAULT);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            ((SimpleCursorAdapter) getListAdapter()).swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    }
}
