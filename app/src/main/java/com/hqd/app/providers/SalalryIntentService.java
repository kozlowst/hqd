package com.hqd.app.providers;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SalalryIntentService extends IntentService {

    public static final String ACTION_INSERT = "com.hqd.app.providers.action.INSERT";
    public static final String ACTION_UPDATE = "com.hqd.app.providers.action.UPDATE";
    public static final String ACTION_DELETE = "com.hqd.app.providers.action.DELETE";


    public SalalryIntentService() {
        super("SalalryIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_INSERT.equals(action)) {
                handleInsert(intent);
            } else if (ACTION_UPDATE.equals(action)) {
                handleUpdate(intent);
            } else if (ACTION_DELETE.equals(action)) {
                handleDelete(intent);
            }
        }
    }

    private void handleInsert(Intent intent) {
        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.d("TAG", " -- Provider has been notified");
            }
        };
        getContentResolver().notifyChange(MyContract.Employees.CONTENT_URI, new DbChangeListener(handler));
        Log.d("TAG", "Intent service received intent: " + intent.getAction());
        String name = intent.getStringExtra(MyContract.Employees.NAME);
        String amount = intent.getStringExtra(MyContract.Employees.AMOUNT);
        ContentValues contentValues = new ContentValues();
        contentValues.put(MyContract.Employees.NAME, name);
        contentValues.put(MyContract.Employees.AMOUNT, amount);
        Uri result = getContentResolver().insert(MyContract.Employees.CONTENT_URI, contentValues);
        if (result == null) {
            Log.e("TAG", "Insert failed");
            return;
        }
        Log.d("TAG", "Added succeeded. Name: " + name + ", Amount: " + amount);
    }

    private void handleUpdate(Intent intent) {

    }

    private void handleDelete(Intent intent) {

    }

}
