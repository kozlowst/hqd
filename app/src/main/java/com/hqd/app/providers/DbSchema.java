package com.hqd.app.providers;

import android.provider.BaseColumns;

/**
 * Created by tomek on 4/5/14.
 */
public interface DbSchema {

    public String DB_NAME = "hqd.db";
    public int VERSION = 2;

    public String TABLE_NAME = "employees";
    public String COLUMN_PERSON_NAME = "name";
    public String COLUMN_SALARY = "salary";

    public String CREATE_TABLE = "CREATE TABLE employees (" +
            "_id    INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            "name   TEXT, \n" +
            "salary INTEGER )";

    public String DROP_TABLE = "DROP TABLE employees";

    public String TABLE_NAME_V1 = "borrowers";
    public String DROP_TABLE_V1 = "DROP TABLE " + TABLE_NAME_V1;

}
