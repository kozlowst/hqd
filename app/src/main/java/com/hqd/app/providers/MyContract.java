package com.hqd.app.providers;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by tomek on 4/5/14.
 */
public final class MyContract {
    public static final String AUTHORITY = "com.hqd.app.provider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final class Employees implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(MyContract.CONTENT_URI, "employees");
        public static final String CONTENT_DIR_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "vnd.com.hqd.app.provider.employees";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "vnd.com.hqd.app.provider.employees";
        public static final String[] PROJECTION_ALL = {_ID, "name", "salary"};
        public static final String SORT_ORDER_DEFAULT = "name ASC";
        public static final String NAME = DbSchema.COLUMN_PERSON_NAME;
        public static final String AMOUNT = DbSchema.COLUMN_SALARY;
    }
}
