package com.hqd.app.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class MyContentProvider extends ContentProvider {

    private MyDbHelper dbHelper = null;
    private static final UriMatcher macher;

    private static final int ITEM_LIST = 1;
    private static final int ITEM_ID = 2;

    static {
        macher = new UriMatcher(UriMatcher.NO_MATCH);
        macher.addURI(MyContract.AUTHORITY, DbSchema.TABLE_NAME, ITEM_LIST);
        macher.addURI(MyContract.AUTHORITY, DbSchema.TABLE_NAME+"/#", ITEM_ID);
    }

    public MyContentProvider() {

    }



    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        switch (macher.match(uri)) {
            case ITEM_ID:
                return MyContract.Employees.CONTENT_ITEM_TYPE;
            case ITEM_LIST:
                return MyContract.Employees.CONTENT_DIR_TYPE;
            default:
                throw new IllegalArgumentException("Uri: " + uri + " not supported.");
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d("TAG", "Provider has received INSERT request");
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (macher.match(uri)) {
            case ITEM_LIST:
                long id = db.insert(DbSchema.TABLE_NAME, null, values);
                return mergeUriWithId(uri, id);
            default:
                throw new IllegalArgumentException("INSERT not supported for Uri: " + uri);
        }
    }

    private Uri mergeUriWithId(Uri uri, long id) {
        if( id<=0) return null;
        getContext().getContentResolver().notifyChange(MyContract.Employees.CONTENT_URI, null);
        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public boolean onCreate() {
        Log.d("TAG", "CIBTEBT PROVIDER !!!!!!");
        dbHelper = new MyDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String table = uri.getLastPathSegment();
        Log.d("TAG", "Query table: " + table );

        return db.query(table, projection, selection, selectionArgs, null, null, sortOrder);

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
