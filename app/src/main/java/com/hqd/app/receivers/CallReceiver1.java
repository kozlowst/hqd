package com.hqd.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class CallReceiver1 extends BroadcastReceiver {
    public CallReceiver1() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "call receiver 1");
        Bundle bundle = getResultExtras(true);
        String txt = bundle.getString("DUPA");
        Log.d("TAG", "Received has received: " + txt);
        MyIntentService.startActionFoo(context, "CallRCV_1", txt);
    }
}
