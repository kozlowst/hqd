package com.hqd.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class CallReceiver2 extends BroadcastReceiver {
    public CallReceiver2() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "call receiver 2");
        Bundle bundle = getResultExtras(true);
        bundle.putString("DUPA", "Hej");
        MyIntentService.startActionBaz(context, "CallRCV_2", "HEHE");
    }
}
