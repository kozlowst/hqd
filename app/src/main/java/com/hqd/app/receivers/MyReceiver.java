package com.hqd.app.receivers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {

    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "Yqhooooooo " + intent.getAction() + ", hc: " + hashCode());
        if(intent.getAction().equals("com.hqd.app.RUN_ASYNC_TASK")) {
            runAsyncTask(System.currentTimeMillis());
        } else if (intent.getAction().equals("com.hqd.app.DO_ANR")) {
            doAnr();
        } else if (intent.getAction().equals("com.hqd.app.DO_TICKS")) {
            doTicks();
        }

    }

    private void doAnr() {
        try {
            int i = 0;
            while(i<6) {
                Thread.sleep(1000);
                Log.d("TAG", "Tick " + i + ", hc:" + hashCode());
                i++;
            }
        } catch (InterruptedException e) {
            Log.e("TAG", "Ups, thread interruption has occurred", e);
        }
    }

    private void doTicks() {
        try {
            int i = 0;
            while(i<5) {
                Thread.sleep(1000);
                Log.d("TAG", " ### Tick " + i + ", hc:" + hashCode());
                i++;
            }
        } catch (InterruptedException e) {
            Log.e("TAG", "Ups, thread interruption has occurred", e);
        }
    }

    private void runAsyncTask(final long timestamp) {
        AsyncTask<Void,Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                runThread(timestamp);
                return null;
            }
        };
        task.execute();
    }

    private void runThread(final long timestamp) {
        Thread th = new Thread() {
            @Override
            public void run() {
                int i=0;
                while (i < 20) {
                    try {
                        Log.d("TAG", "TRIGGGGGG at " + timestamp);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e("TAG", "Fuckup", e);
                        return;
                    }
                    i++;
                }
            }
        };

        th.start();
    }
}
