package com.hqd.app.receivers;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.util.Log;

public class MyService extends Service {

    private final IBinder binder = new LocalBinder();
    private Thread th = null;

    public class LocalBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

    public MyService() {
    }

    @Override
    public void onCreate() {
        Log.d("TAG", " -- onCreate");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("TAG", " -- onStartCommand");
        doJob();
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        Log.d("TAG", " -- onDestroy");
        calncalJob();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("TAG", "onBind");
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("TAG", "onUnbind");
        return super.onUnbind(intent);
    }

    public void doJob() {
        th = new Thread() {
            @Override
            public void run() {
                try {
                    int i = 0;
                    while(i<20) {
                        Thread.sleep(1000);
                        Log.d("TAG", "Tick " + i + ", id:" + getId() + ", prio:" + getPriority());
                        i++;
                    }
                    stopSelf();
                } catch (InterruptedException e) {
                    Log.e("TAG", "Ups, thread interruption has occurred", e);
                }
            }
        };
        Log.d("TAG", "Thread ID: " + th.getId());
        // TODO: SecurityException: No permission to modify given thread -- why?
        //android.os.Process.setThreadPriority((int) th.getId(), Process.THREAD_PRIORITY_BACKGROUND);
        th.start();
    }

    public void calncalJob() {
        Log.d("TAG", "Cancel job");
        if (th != null) {
            th.interrupt();
        }
    }
}
