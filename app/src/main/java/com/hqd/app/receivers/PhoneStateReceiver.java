package com.hqd.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PhoneStateReceiver extends BroadcastReceiver {
    public PhoneStateReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "PhoneSTate " + intent.getAction() + ", " + intent.getData() + ",  " + intent.getPackage());
    }
}
