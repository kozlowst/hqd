package com.hqd.app.receivers;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.inject.Inject;
import com.hqd.app.R;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class TimerActivity extends RoboActivity {

    @InjectView(R.id.timeText)  private EditText time;
    @InjectView(R.id.startTimer) private Button startTimer;
    @InjectView(R.id.sendBroadcast) private Button sendBroadcasrButton;
    @InjectView(R.id.sendBroadcast2) private Button sendBroadcasrButton2;
//    @InjectView(R.id.bindToService) private Button bindToService;
//    @InjectView(R.id.unbindService) private Button unbindFromService;
//    @InjectView(R.id.callServiceMethod) private Button callServiceMethod;
    @InjectView(R.id.startService) private Button startService;
    @InjectView(R.id.startEditApp) private Button startEditApp;
    @InjectView(R.id.sendLocal) private Button sendLocal;
    @InjectView(R.id.unregisterLocal) private Button unregisterLocal;
    @Inject private AlarmManager alarmManager;
    private MyService service = null;

    private BroadcastReceiver receiver = new MyReceiver();

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            MyService.LocalBinder myBinder = (MyService.LocalBinder) binder;
            service = myBinder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            service = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        startTimer.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View view) {
                Log.d("TAG", "Staring timer");
                kickReceivedInOrder();
                Intent intent = new Intent("com.hqg.status");
                intent.putExtra("STTATUSIK", 34);
                TimerActivity.this.sendStickyBroadcast(intent);
                MyIntentService.startActionBaz(TimerActivity.this, "zero", "Jeden");
                MyIntentService.startActionFoo(TimerActivity.this, "Jeko", "Ghjg");
                Log.d("TAG", "All services and receivers kicked");

                Intent itt = TimerActivity.this.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                Log.d("TAG", "STICKY INTENT RCVD: " + itt.getAction() + ", " + itt.getExtras().getInt(BatteryManager.EXTRA_LEVEL));

                Intent ittSTatusik = TimerActivity.this.registerReceiver(null, new IntentFilter("com.hqg.status"));
                Log.d("TAG", "STTATUSIK: " + ittSTatusik.getAction() + ", " + ittSTatusik.getExtras().getInt("STTATUSIK"));

                if (!time.getText().toString().isEmpty()) {
                    int timerVal = Integer.parseInt(time.getText().toString());
                    Intent intent2 = new Intent(TimerActivity.this, MyReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(TimerActivity.this, 12345, intent2, 0);
                    if(pendingIntent != null) {
                        try {
                            pendingIntent.send();
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    }
                    //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (timerVal*1000), pendingIntent);
                    Log.d("TAG", "Timer triggered");
                }
            }
        });

        sendBroadcasrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast(new Intent("com.hqd.app.RUN_ASYNC_TASK"));
            }
        });

        sendBroadcasrButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast(new Intent("com.hqd.app.DO_ANR"));
            }
        });

//        bindToService.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(TimerActivity.this, MyService.class);
//                bindService(intent, connection, Context.BIND_AUTO_CREATE);
//            }
//        });
//
//        unbindFromService.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                unbindService(connection);
//            }
//        });
//
//        callServiceMethod.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (service != null) {
//                    service.doJob();
//                }
//            }
//        });

        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TimerActivity.this, MyService.class);
                startService(intent);
            }
        });

        startEditApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "Register receiver");
                //LocalBroadcastManager manager = LocalBroadcastManager.getInstance(TimerActivity.this);
                //manager.registerReceiver(receiver, new IntentFilter("com.hqd.app.DO_TICKS"));
                TimerActivity.this.registerReceiver(receiver, new IntentFilter("com.hqd.app.DO_TICKS"));
            }
        });

        sendLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "Send broadcast");
                //LocalBroadcastManager.getInstance(TimerActivity.this).sendBroadcast(new Intent("com.hqd.app.DO_TICKS"));
                TimerActivity.this.sendBroadcast(new Intent("com.hqd.app.DO_ANR"));
                Log.d("TAG", "Broadcast sent");
            }
        });

        unregisterLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "Unregister DO_TICKS");
                //LocalBroadcastManager.getInstance(TimerActivity.this).unregisterReceiver(receiver);
                TimerActivity.this.unregisterReceiver(receiver);
            }
        });
    }

    @Override
    protected void onStart() {
        Log.d("TAG", "TimerActivity -- onStart");
        super.onStart();
    }

    @Override
    protected void onPause() {
        Log.d("TAG", "TimerActivity -- onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.d("TAG", "TimerActivity -- onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.d("TAG", "TimerActivity -- onResume");
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.d("TAG", "TimerActivity -- onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("TAG", "TimerActivity -- onDestroy");
        super.onDestroy();
    }

    private void kickReceivedInOrder() {
        Intent intent = new Intent("com.hqd.action");
        sendOrderedBroadcast(intent, null);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
