package com.hqd.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by tomek on 4/4/14.
 */
public class HqdApplication extends Application {
    private static Context context;
    private static HqdApplication instance;

    public Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        instance = new HqdApplication();
    }


}
