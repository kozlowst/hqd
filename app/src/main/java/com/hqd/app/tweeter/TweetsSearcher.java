package com.hqd.app.tweeter;

import android.util.Log;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Created by tomek on 3/8/14.
 */
public class TweetsSearcher {

    public void search() throws TwitterException {
        // The factory instance is re-useable and thread safe.
        Twitter twitter = TwitterFactory.getSingleton();
        Query query = new Query("source:twitter4j yusukey");
        QueryResult result = twitter.search(query);
        for (Status status : result.getTweets()) {
            //System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
            Log.d("Tweeter", "@" + status.getUser().getScreenName() + ":" + status.getText());
        }
    }

}
