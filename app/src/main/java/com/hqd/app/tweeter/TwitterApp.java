package com.hqd.app.tweeter;

import twitter4j.DirectMessage;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hqd.app.R;

import java.util.List;

/**
 * Created by tomek on 3/8/14.
 */
public class TwitterApp extends Activity implements View.OnClickListener {
    private static final String TAG = "TWITTAPP";

    private Button searchButton;
    private Button buttonLogin;
    private Button getTweetButton;
    private TextView tweetText;
    private ScrollView scrollView;

    private static Twitter twitter;
    private static RequestToken requestToken;
    private static SharedPreferences mSharedPreferences;
    private static TwitterStream twitterStream;
    private boolean running = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweeter_main);

        mSharedPreferences = getSharedPreferences(Const.PREFERENCE_NAME, MODE_PRIVATE);
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        tweetText =(TextView)findViewById(R.id.tweetText);
        getTweetButton = (Button)findViewById(R.id.getTweet);
        getTweetButton.setOnClickListener(this);
        buttonLogin = (Button) findViewById(R.id.twitterLogin);
        buttonLogin.setOnClickListener(this);

        searchButton = (Button) findViewById(R.id.twitterSearch);
        searchButton.setOnClickListener(this);

        Uri uri = getIntent().getData();
        Log.d(TAG, "Uri: " + uri);
        if (uri != null && uri.toString().startsWith(Const.CALLBACK_URL)) {
            String verifier = uri.getQueryParameter(Const.IEXTRA_OAUTH_VERIFIER);
            try {
                AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                Log.d(TAG, "Token secret: " + accessToken.getToken());
                Editor e = mSharedPreferences.edit();
                e.putString(Const.PREF_KEY_TOKEN, accessToken.getToken());
                e.putString(Const.PREF_KEY_SECRET, accessToken.getTokenSecret());
                e.commit();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    protected void onResume() {
        super.onResume();

        if (isConnected()) {
            String oauthAccessToken = mSharedPreferences.getString(Const.PREF_KEY_TOKEN, "");
            String oAuthAccessTokenSecret = mSharedPreferences.getString(Const.PREF_KEY_SECRET, "");

            ConfigurationBuilder confbuilder = new ConfigurationBuilder();
            Configuration conf = confbuilder
                    .setOAuthConsumerKey(Const.CONSUMER_KEY)
                    .setOAuthConsumerSecret(Const.CONSUMER_SECRET)
                    .setOAuthAccessToken(oauthAccessToken)
                    .setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
                    .build();
            twitterStream = new TwitterStreamFactory(conf).getInstance();

            buttonLogin.setText(R.string.label_disconnect);
            getTweetButton.setEnabled(true);
        } else {
            buttonLogin.setText(R.string.label_connect);
        }
    }

    private boolean isConnected() {
        return mSharedPreferences.getString(Const.PREF_KEY_TOKEN, null) != null;
    }

    private void askOAuth() {

        AuthTask task = new AuthTask();
        task.execute((Void) null);

    }

    private void disconnectTwitter() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(Const.PREF_KEY_TOKEN);
        editor.remove(Const.PREF_KEY_SECRET);

        editor.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.twitterLogin:
                if (isConnected()) {
                    disconnectTwitter();
                    buttonLogin.setText(R.string.label_connect);
                } else {
                    askOAuth();
                }
                break;
            case R.id.getTweet:
                if (running) {
                    stopStreamingTimeline();
                    running = false;
                    getTweetButton.setText("start streaming");
                } else {
                    startStreamingTimeline();
                    running = true;
                    getTweetButton.setText("stop streaming");
                }
                break;
            case R.id.twitterSearch:
                Thread thread = new Thread() {

                    @Override
                    public void run() {
                        Query query = new Query("#sun");
                        QueryResult queryResult;
                        try {
                            queryResult = twitter.search(query);
                            List<Status> tweets = queryResult.getTweets();
                            int i=0;
                            for (Status tweet : tweets) {
                                final String text="@" + tweet.getUser().getScreenName() + " date:" + tweet.getCreatedAt() + ", text: " + tweet.getText() + "\n\n";
                                tweetText.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        tweetText.append(text);
                                        scrollView.fullScroll(View.FOCUS_DOWN);
                                    }
                                });

                                Log.d(TAG, " ###### Tweet: @" + tweet.getUser().getScreenName() + ", " + tweet.getSource() + ", txt: " + tweet.getText());
                                if (++i > 20) {
                                    break;
                                }
                            }
                        } catch (TwitterException e) {
                            Log.e(TAG, "Error when query execution");
                        }
                    }
                };
                thread.start();
                break;
        }
    }

    private void stopStreamingTimeline() {
        twitterStream.shutdown();
    }

    public void startStreamingTimeline() {
        UserStreamListener listener = new UserStreamListener() {

            @Override
            public void onDeletionNotice(StatusDeletionNotice arg0) {

            }

            @Override
            public void onScrubGeo(long arg0, long arg1) {

            }

            @Override
            public void onStatus(Status status) {
                final String tweet = "@" + status.getUser().getScreenName() + " : " + status.getText() + "\n";
                System.out.println(tweet);
                Log.d(TAG, " ++ Tweet: " + tweet);
//                tweetText.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        tweetText.append(tweet);
//                        scrollView.fullScroll(View.FOCUS_DOWN);
//                    }
//                });
            }

            @Override
            public void onTrackLimitationNotice(int arg0) {
                System.out.println("trackLimitation");
            }

            @Override
            public void onException(Exception arg0) {

            }

            @Override
            public void onBlock(User arg0, User arg1) {
                Log.d(TAG, " ++ onBlock");
            }

            @Override
            public void onDeletionNotice(long arg0, long arg1) {
                Log.d(TAG, " ++ onDeletionNotice");
            }

            @Override
            public void onDirectMessage(DirectMessage arg0) {
                Log.d(TAG, "onDirectMessage");
            }

            @Override
            public void onFavorite(User arg0, User arg1, Status arg2) {
                Log.d(TAG, "onFavorite");
            }

            @Override
            public void onFollow(User source, User followedUser) {
                Log.d(TAG, "Source: " + source.getName() + ", followed: " + followedUser.getName());
            }

            @Override
            public void onFriendList(long[] arg0) {
                Log.d(TAG, "onFriendList");
            }

            @Override
            public void onUnblock(User arg0, User arg1) {
                Log.d(TAG, "onUnblock");
            }

            @Override
            public void onUnfavorite(User arg0, User arg1, Status arg2) {
                Log.d(TAG, "onUnfavorite");
            }

            @Override
            public void onUserListCreation(User arg0, UserList arg1) {
                Log.d(TAG, "onUserListCreation " + arg0.getName());
            }

            @Override
            public void onUserListDeletion(User arg0, UserList arg1) {
                Log.d(TAG, "onUserListDeletion " + arg0.getName());
            }

            @Override
            public void onUserListMemberAddition(User arg0, User arg1, UserList arg2) {
                Log.d(TAG, "onUserListMemberAdd " + arg0.getName() + ", " + arg1.getName());
            }

            @Override
            public void onUserListMemberDeletion(User arg0, User arg1, UserList arg2) {
                Log.d(TAG, "onUserListMemberDel " + arg0.getName() + ", " + arg1.getName());
            }

            @Override
            public void onUserListSubscription(User arg0, User arg1, UserList arg2) {
                Log.d(TAG, "onUserListMemberSub " + arg0.getName() + ", " + arg1.getName());
            }

            @Override
            public void onUserListUnsubscription(User arg0, User arg1, UserList arg2) {
                Log.d(TAG, "onUserListMemberUnsub " + arg0.getName() + ", " + arg1.getName());
            }

            @Override
            public void onUserListUpdate(User arg0, UserList arg1) {
                Log.d(TAG, "onUserListUpdate " + arg0.getName() + ", " + arg1.getName());
            }

            @Override
            public void onUserProfileUpdate(User arg0) {
                Log.d(TAG, "onUserPrifileUpdate " + arg0.getName());
            }

            @Override
            public void onStallWarning(StallWarning arg0) {


            }
        };
        twitterStream.addListener(listener);
        twitterStream.user();
    }


    private class AuthTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.setOAuthConsumerKey(Const.CONSUMER_KEY);
            configurationBuilder.setOAuthConsumerSecret(Const.CONSUMER_SECRET);
            Configuration configuration = configurationBuilder.build();

            TwitterFactory tf = new TwitterFactory(configuration);
            Twitter tw = tf.getInstance();
            Log.d(TAG, tw.toString());

//            twitter = new TwitterFactory(configuration).getInstance();
//
            try {
                requestToken = tw.getOAuthRequestToken(Const.CALLBACK_URL);
                twitter = tw;
            } catch (Exception e) {
                Log.e(TAG, "Twitter has fucked up!!");
                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean flag) {
            super.onPostExecute(flag);
            if(flag) {
                TwitterApp.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
            } else {
                Toast.makeText(TwitterApp.this, "Already Logged into twitter", Toast.LENGTH_LONG).show();
            }
        }

    };
}
