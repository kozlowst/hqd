package com.hqd.app.rss;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hqd.app.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import roboguice.activity.RoboListActivity;
import roboguice.inject.InjectView;

/**
 * Created by tomek on 3/8/14.
 */
public class RssActivity extends RoboListActivity {

    private static final String LOGTAG = "RssFeed";

    private @InjectView(R.id.rss_form) View mRssView;
    private @InjectView(R.id.rss_progress) View mRssProgressView;

    private UserLoginTask mAuthTask = null;
    private String feedUrl;
    private Map<String, InfoData> rssFeeds = new HashMap<String, InfoData>();
    private ArrayList<String> listItems=new ArrayList<String>();
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            feedUrl = b.getString("url");
            Log.d(LOGTAG, "onCreate: url= " + feedUrl);
        }
        attemptRssFeedsReading();
        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        setListAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rss, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                break;
            case R.id.refresh:
                attemptRssFeedsReading();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    public void attemptRssFeedsReading() {

        Log.d(LOGTAG, "attemptRssFeedsReading");

        if (mAuthTask != null) {
            return;
        }

        showProgress(true);
        mAuthTask = new UserLoginTask(feedUrl);
        mAuthTask.execute((Void) null);
    }

    public void updateItems() {
        listItems.clear();
        for (String item : rssFeeds.keySet()) {
            listItems.add(item);
        }
        adapter.notifyDataSetChanged();
        Log.d(LOGTAG, "LIST: " + listItems.toString());

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRssView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRssView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRssView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mRssProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRssProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRssProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mRssProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRssView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        //super.onListItemClick(l, v, position, id);
        String title = listItems.get(position);
        InfoData infoData = rssFeeds.get(title);
        Intent intent = new Intent(RssActivity.this, RssCurrencyActivity.class);
        Bundle b = new Bundle();
        b.putString("title", title);
        b.putString("date", infoData.pubDate);
        b.putString("description", infoData.description);
        intent.putExtras(b);
        startActivity(intent);
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUrl;

        UserLoginTask(String url) {
            mUrl = url;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            RssReader reader = new RssReader();
            reader.readFeed(mUrl);
            //Log.d(LOGTAG, reader.getFeed().toString());

            rssFeeds.clear();
            for (InfoData infoData : reader.getFeed()) {
                rssFeeds.put(infoData.title, infoData);
            }
            Log.d(LOGTAG, rssFeeds.toString());
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                RssActivity.this.updateItems();
                //finish();
            } else {

            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}



