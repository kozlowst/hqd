package com.hqd.app.rss;

/**
 * Created by tomek on 3/8/14.
 */
public class InfoData {

    public String title;
    public String link;
    public String description;
    public String pubDate;

    public InfoData() {
    }

    public InfoData(InfoData infoData) {
        this.title = infoData.title;
        this.link = infoData.link;
        this.description = infoData.description;
        this.pubDate = infoData.pubDate;
    }

    @Override
    public String toString() {
        StringBuilder stb = new StringBuilder();
        stb.append("Title: ").append(title).append("\n  link: ").append(link).append("\n  ").append(description).append("\n\n");
        return stb.toString();
    }
}
