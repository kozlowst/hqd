package com.hqd.app.rss;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hqd.app.R;

import roboguice.activity.RoboListActivity;

/**
 * Created by tomek on 3/8/14.
 */
public class MainRssActivity extends RoboListActivity {

    private ArrayAdapter<String> adapter;
    private String[] values;
    private String[] urls;

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Log.d("RSS", "RSS clicked: " + position + ": " + values[position] + ", url: " + urls[position]);
        Intent intent = new Intent(MainRssActivity.this, RssActivity.class);
        Bundle b = new Bundle();
        b.putString("url", urls[position]);
        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_rss);
        Resources res = this.getResources();
        values = res.getStringArray(R.array.countries_array);
        urls = res.getStringArray(R.array.urls_array);
        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                values);
        setListAdapter(adapter);
    }
}
