package com.hqd.app.rss;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by tomek on 3/8/14.
 */
public class RssHandler extends DefaultHandler {

    private StringBuffer chars = new StringBuffer();
    public Feed feed = new Feed();
    private boolean isChannelProcessing = false;
    private boolean isItemProcessing = false;
    private InfoData infoData = new InfoData();
    private InfoData channelInfoData = new InfoData();

    @Override
    public void characters(char ch[], int start, int length) {
        chars.append(new String(ch, start, length));
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        //System.out.println("-- start :: localName: " + localName + ", qName: " +qName );
        for(int i=0; i<atts.getLength(); i++) {
            System.out.println("---- " + atts.getValue(i));
        }

        if(qName.equals("channel")) {
//           System.out.println("- start channel");
            isChannelProcessing=true;
        } else if(qName.equals("item")) {
            if(isChannelProcessing & !isItemProcessing) {
                channelInfoData = new InfoData(infoData);
            }
//            System.out.println("- start item");
            isItemProcessing=true;
            infoData = new InfoData();
        }
        chars = new StringBuffer();
    }



    @Override

    public void endElement(String uri, String localName, String qName) throws SAXException {
        //System.out.println("-- end :: localName: " + localName + ", qName: " +qName +  ", chrs: " + chars.toString());
        if (qName.equals("title")) {
            infoData.title = chars.toString();
        } else if (qName.equals("link")) {
            infoData.link = chars.toString();
        } else if (qName.equals("description")) {
            infoData.description = chars.toString();
        } else if (qName.equals("pubDate")) {
            infoData.pubDate = chars.toString();
        }

//        System.out.println("-- " + infoData.toString());

        if(qName.equals("item") && isItemProcessing) {
            feed.addChannelItem(infoData);
        } else if(isChannelProcessing) {
            feed.setChannelInfo(channelInfoData);
        }
    }
}
