package com.hqd.app.rss;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomek on 3/8/14.
 */
public class Feed {

    private InfoData channelInfo;

    private List<InfoData> items = new ArrayList<InfoData>();

    public InfoData getChannelInfo() {
        return channelInfo;
    }

    public List<InfoData> getChannelItems() {
        return items;
    }

    public void addChannelItem(InfoData itemData) {
        items.add(itemData);
    }

    public void setChannelInfo(InfoData channelInfo) {
        this.channelInfo = channelInfo;
    }

}
