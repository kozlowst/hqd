package com.hqd.app.rss;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hqd.app.R;

import java.util.ArrayList;
import java.util.List;

public class RssLoaderActivity extends ActionBarActivity {

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_loader);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new CurrencyListFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rss_loader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void showSelectedCurrency() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new CurrencyLoaderFagment(url));
        //transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class CurrencyListFragment extends ListFragment {

        private String [] urls;

        public CurrencyListFragment() {
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            Resources res = getResources();
            urls = res.getStringArray(R.array.urls_array);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, res.getStringArray(R.array.countries_array));
            setListAdapter(adapter);
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            String url = urls[position];
            Log.d("TAG", "Selected url: " + url);
            ((RssLoaderActivity) getActivity()).setUrl(url);
            ((RssLoaderActivity) getActivity()).showSelectedCurrency();
        }
    }

    public static class CurrencyLoaderFagment extends ListFragment implements LoaderManager.LoaderCallbacks<List<InfoData>>{

        private String url;
        private AsyncTaskLoader<List<InfoData>> loader;

        public CurrencyLoaderFagment() {

        }

        public CurrencyLoaderFagment(String url) {
            this.url = url;
        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            loader = new AsyncTaskLoader<List<InfoData>>(getActivity()) {
                @Override
                public List<InfoData> loadInBackground() {
                    Log.d("TAG", "TaskLoader kicked");
                    RssReader reader = new RssReader();
                    reader.readFeed(url);
                    return reader.getFeed();
                }
            };
            setListAdapter(new InfoDataAdapter(getActivity(),R.layout.info_data_layout, new ArrayList<InfoData>()));
            getLoaderManager().initLoader(123, null, this).forceLoad();
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
        }

        @Override
        public Loader<List<InfoData>> onCreateLoader(int id, Bundle args) {
            Log.d("TAG", "Create loader");
            return loader;
        }

        @Override
        public void onLoadFinished(Loader<List<InfoData>> loader, List<InfoData> data) {
            Log.d("TAG", "Loader has finished");
            setListAdapter(new InfoDataAdapter(getActivity(), R.layout.info_data_layout, data));
        }

        @Override
        public void onLoaderReset(Loader<List<InfoData>> loader) {
            Log.d("TAG", "Loader reset");
        }
    }
}
