package com.hqd.app.rss;

import android.os.Bundle;
import android.widget.TextView;

import com.hqd.app.R;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

/**
 * Created by tomek on 3/8/14.
 */
public class RssCurrencyActivity extends RoboActivity {

    private @InjectView(R.id.rss_currency_title) TextView mTitle;
    private @InjectView(R.id.rss_date) TextView mDate;
    private @InjectView(R.id.rss_currency) TextView mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_currency);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            mTitle.setText(b.getString("title"));
            mDate.setText(b.getString("date"));
            mDescription.setText(b.getString("description"));
        }

    }
}
