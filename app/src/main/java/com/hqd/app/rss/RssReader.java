package com.hqd.app.rss;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by tomek on 3/8/14.
 */
public class RssReader {

    private RssHandler rh;

    public List<InfoData> getFeed() {
        return new ArrayList<InfoData>(rh.feed.getChannelItems());
    }

    public void readFeed(String feedUrl) {

        URL url = null;
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp;

        try {
            sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            url = new URL(feedUrl);
            rh = new RssHandler();
            xr.setContentHandler(rh);
            xr.parse(new InputSource(url.openStream()));

//            System.out.println("Channel: " + rh.feed.getChannelInfo().toString());
//            for(InfoData info : rh.feed.getChannelItems()) {
//                System.out.println(info.toString());
//            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
