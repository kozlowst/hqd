package com.hqd.app.rss;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hqd.app.R;

import java.util.List;

/**
 * Created by tomek on 4/5/14.
 */
public class InfoDataAdapter extends ArrayAdapter<InfoData> {

    private LayoutInflater layoutInflater;
    private List<InfoData> infoDataList;
    private int res;

    public InfoDataAdapter(Context context, int resource, List<InfoData> objects) {
        super(context, resource, objects);
        infoDataList = objects;
        res = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return infoDataList.size();
    }

    @Override
    public InfoData getItem(int position) {
        return infoDataList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = layoutInflater.inflate(res, null);

        TextView textView = (TextView) convertView.findViewById(R.id.infoPubDate);
        textView.setText(infoDataList.get(position).pubDate);

        TextView desc = (TextView) convertView.findViewById(R.id.infoDescription);
        desc.setText(infoDataList.get(position).description);

        return convertView;
    }
}
