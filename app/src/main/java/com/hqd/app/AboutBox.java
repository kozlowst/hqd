package com.hqd.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by tomek on 3/8/14.
 */
public class AboutBox {
    static String VersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "Version unknown";
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void Show(Activity activity) {
        String text = String.format(activity.getString(R.string.about));
        CharSequence styledText = Html.fromHtml(text);

        SpannableString aboutText = new SpannableString("Version " + VersionName(activity) + "\n\n" + styledText);
        View aboutView;
        TextView aboutTextView;

        try {
            LayoutInflater inflater = activity.getLayoutInflater();
            aboutView = inflater.inflate(R.layout.about, (ViewGroup) activity.findViewById(R.id.about_view));
            aboutTextView = (TextView) aboutView.findViewById(R.id.about_text);
        } catch (InflateException e) {
            aboutView = aboutTextView = new TextView(activity);
        }

        aboutTextView.setText(aboutText);
        Linkify.addLinks(aboutTextView, Linkify.ALL);

        new AlertDialog.Builder(activity, android.R.style.Theme)
                .setTitle("About " + activity.getString(R.string.app_name))
                .setCancelable(true)
                .setIcon(R.drawable.ic_launcher)
                .setPositiveButton("OK", null)
                .setView(aboutView)
                .show();
    }

}
