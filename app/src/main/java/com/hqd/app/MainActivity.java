package com.hqd.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hqd.app.barplot.BarPlotActivity;
import com.hqd.app.loaders.CallsHistoryActivity;
import com.hqd.app.providers.EmployeesActivity;
import com.hqd.app.receivers.TimerActivity;
import com.hqd.app.rss.MainRssActivity;
import com.hqd.app.rss.RssLoaderActivity;
import com.hqd.app.tweeter.TweetsSearcher;
import com.hqd.app.tweeter.TwitterApp;

import java.util.ArrayList;

import twitter4j.TwitterException;

public class MainActivity extends ActionBarActivity {

    private static final String EXCHANGE_RATES = "Exchange";
    private static final String EXCHANGE_RATES2 = "Exchange2";
    private static final String STOP_CUBE = "Stop cube!";
    private static final String TWEETER = "Tweeter";
    private static final String PROVIDERS = "Providers";
    private static final String BARPLOT = "BarPlot";

    GridView gridView;
    ArrayList<Item> gridArray = new ArrayList<Item>();
    CustomGridViewAdapter customGridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bitmap currencyIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.conversion_of_currency);
        Bitmap gameIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.joystick);
        Bitmap tweeterIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.twitter_bird);
        Bitmap providersIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher);


        gridArray.add(new Item(currencyIcon,EXCHANGE_RATES));
        gridArray.add(new Item(currencyIcon,EXCHANGE_RATES2));
        gridArray.add(new Item(gameIcon,STOP_CUBE));
        gridArray.add(new Item(tweeterIcon, TWEETER));
        gridArray.add(new Item(providersIcon, PROVIDERS));
        gridArray.add(new Item(providersIcon, BARPLOT));


        gridView = (GridView) findViewById(R.id.gridView1);
        customGridAdapter = new CustomGridViewAdapter(this, R.layout.row_grid, gridArray);
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Item item = customGridAdapter.getItem(position);
                Intent intent;
                if (item.getTitle().equals(EXCHANGE_RATES)) {
                    intent = new Intent(MainActivity.this, MainRssActivity.class);
                    startActivity(intent);
                } else if (item.getTitle().equals(STOP_CUBE)) {
                    intent = new Intent(MainActivity.this, RenderActivity.class);
                    startActivity(intent);
                } else if (item.getTitle().equals(TWEETER)) {
                    //intent = new Intent(MainActivity.this, TwitterApp.class);
                    intent = new Intent(MainActivity.this, TimerActivity.class);
                    startActivity(intent);
                } else if (item.getTitle().equals(PROVIDERS)) {
                    intent = new Intent(MainActivity.this, EmployeesActivity.class);
                    startActivity(intent);
                } else if(item.getTitle().equals(EXCHANGE_RATES2)) {
                    intent = new Intent(MainActivity.this, RssLoaderActivity.class);
                    startActivity(intent);
                } else if (item.getTitle().equals(BARPLOT)) {
                    intent = new Intent(MainActivity.this, BarPlotActivity.class);
                    startActivity(intent);
                }

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                break;
            case R.id.about:
                AboutBox.Show(MainActivity.this);
                break;
        }

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
