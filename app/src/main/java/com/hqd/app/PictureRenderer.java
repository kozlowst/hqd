package com.hqd.app;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by tomek on 3/8/14.
 */
public class PictureRenderer implements GLSurfaceView.Renderer, SensorEventListener {

    private Cube cube = new Cube();
    private Cube2 cube2 = new Cube2(1, 1, 1);
    private float cubeRotation;

    private float x,y,z;
    private double g;

    private float colors[] = {
            1.0f, 1.0f, 1.0f, 1.0f,
            0.0f, 0.5f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            0.0f, 0.5f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            0.0f, 0.5f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            0.0f, 0.5f, 0.0f, 1.0f

    };

    float[] clr = new float[colors.length];

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        gl10.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
        gl10.glClearDepthf(1.0f);
        gl10.glEnable(GL10.GL_DEPTH_TEST);
        gl10.glDepthFunc(GL10.GL_LEQUAL);
        gl10.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        cube2.rx = 45;
        cube2.ry = 45;
        cube2.x=0;
        cube2.y=0;
        cube2.z=-10;
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        gl10.glViewport(0, 0, width, height);
        gl10.glMatrixMode(GL10.GL_PROJECTION);
        gl10.glLoadIdentity();
        GLU.gluPerspective(gl10, 45.0f, (float) width / (float) height, 0.1f, 100.0f);
        gl10.glViewport(0, 0, width, height);

        gl10.glMatrixMode(GL10.GL_MODELVIEW);
        gl10.glLoadIdentity();
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl10.glLoadIdentity();
        //gl10.glTranslatef(0.0f, 0.0f, -10.0f);
        //gl10.glRotatef(cubeRotation, 1.0f, 1.0f, 1.0f);
        cube2.move(gl10);
        cube2.draw(gl10);
        gl10.glLoadIdentity();
        cube2.rx +=x;
        cube2.ry +=y;
        cube2.rz +=z;
        cubeRotation -= 1.5f;
        //cube2.setColors(createNewColors());
        //cube2.updateColors(colors);

//        gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
//        gl10.glLoadIdentity();
//        gl10.glTranslatef(0.0f, 0.0f, -10.0f);
//        gl10.glRotatef(cubeRotation, 1.0f, 1.0f, 1.0f);
//
//        cube.draw(gl10);
//
//        gl10.glLoadIdentity();
//        cubeRotation -= 1.5f;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        x = sensorEvent.values[0]/5;
        y = sensorEvent.values[1]/5;
        z = sensorEvent.values[2]/5;
        g = Math.sqrt(x * x + y * y + z * z)/SensorManager.GRAVITY_EARTH;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private float[] createNewColors() {
        for (int i=0; i<colors.length; i++) {
            float n;
            if(g/10>0.5){
                n=0.5f;
            } else {
                n= (float) g;
            }
            if(i==5 || i==13 || i==21 || i==29) {
                //clr[i] = colors[i] + n;
            } else {
                clr[i] = colors[i];
            }
        }
        return clr;
    }
}
