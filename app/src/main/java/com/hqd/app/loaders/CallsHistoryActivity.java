package com.hqd.app.loaders;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hqd.app.R;

public class CallsHistoryActivity extends FragmentActivity {

    public static class DetailsActivity extends FragmentActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.call_fragment_layout);

            Log.d("TAG", "DetailsActivity onCreate");

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                Log.d("TAG", "out");
                finish();
                return;
            }

            if (savedInstanceState == null) {
                Log.d("TAG", "run fragment");
                DetailsFragment details = new DetailsFragment();
                details.setArguments(getIntent().getExtras());
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(android.R.id.content, details).commit();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_fragment_layout);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calls_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class DetailsFragment extends Fragment {

        public static DetailsFragment newInstance(int pos) {
            DetailsFragment df = new DetailsFragment();

            Bundle bundle = new Bundle();
            bundle.putInt("pos", pos);
            df.setArguments(bundle);
            return df;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            int pos = getArguments().getInt("pos", 0);
            Log.d("TAG", "DetailFragment createView, pos= " + pos);
            ScrollView scrollView = new ScrollView(getActivity());
            TextView text = new TextView(getActivity());
            int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    4, getActivity().getResources().getDisplayMetrics());
            text.setPadding(padding, padding, padding, padding);
            scrollView.addView(text);
            text.setText(SomeData.DETAILS[pos]);
            return scrollView;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class NamesFragment extends ListFragment {

        private boolean isDualMode;
        private int currPos = 0;
        private int shownPos = -1;


        public NamesFragment() {
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt("curr", currPos);
            outState.putInt("shown", shownPos);
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            showDetails(position);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            if (savedInstanceState != null) {
                currPos = savedInstanceState.getInt("curr");
                shownPos = savedInstanceState.getInt("shown");
            }

            setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, SomeData.NAMES));

            View details = getActivity().findViewById(R.id.details);
            if (details != null && details.getVisibility() == View.VISIBLE) {
                isDualMode = true;
                getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                showDetails(currPos);
            } else {
                isDualMode = false;
            }

        }


        private void showDetails(int pos) {
            if (isDualMode) {
                Log.d("TAG", "showDetails: dualMode");
                getListView().setItemChecked(pos, true);
                if (pos != shownPos) {
                    DetailsFragment df = DetailsFragment.newInstance(pos);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.details, df);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                    shownPos = pos;
                }
            } else {
                Log.d("TAG", "showDetauls: not dual mode");
                //Intent intent = new Intent(getActivity(), DetailsActivity.class);
                Intent intent = new Intent();
                intent.setClass(getActivity(), DetailsActivity.class);
                intent.putExtra("pos", pos);
                startActivity(intent);

            }
        }


    }


}
