package com.hqd.app.loaders;

/**
 * Created by tomek on 4/2/14.
 */
public class SomeData {
    public static final String NAMES[] = {
            "Tomek",
            "Krzysiek",
            "Magda",
            "Mikolaj"
    };

    public static final String DETAILS[] = {
            "Cool guy, tries to be a geek",
            "Nice guy, Tomek's brother",
            "Cool girl, she is Tomek's daughter",
            "Young geek guy, big fun of Star wars"
    };
}
