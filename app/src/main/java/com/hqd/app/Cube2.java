package com.hqd.app;

/**
 * Created by tomek on 3/7/14.
 */
public class Cube2 extends BaseCont {
    public Cube2(float width, float height, float depth) {
        width  /= 2;
        height /= 2;
        depth  /= 2;

        float vertices[] = { -width, -height, -depth, // 0
                width, -height, -depth, // 1
                width,  height, -depth, // 2
                -width,  height, -depth, // 3
                -width, -height,  depth, // 4
                width, -height,  depth, // 5
                width,  height,  depth, // 6
                -width,  height,  depth, // 7
        };

//        float vertices[] = {
//                -1.0f, -1.0f, -1.0f,
//                1.0f, -1.0f, -1.0f,
//                1.0f, 1.0f, -1.0f,
//                -1.0f, 1.0f, -1.0f,
//                -1.0f, -1.0f, 1.0f,
//                1.0f, -1.0f, 1.0f,
//                1.0f, 1.0f, 1.0f,
//                -1.0f, 1.0f, 1.0f,
//        };

//        short indices[] =
//              { 0, 4, 5,
//                0, 5, 1,
//                1, 5, 6,
//                1, 6, 2,
//                2, 6, 7,
//                2, 7, 3,
//                3, 7, 4,
//                3, 4, 0,
//                4, 7, 6,
//                4, 6, 5,
//                3, 0, 1,
//                3, 1, 2, };

        short indices[] = {
                0,4,5,0,5,1,
                1,5,6,1,6,2,
                2,6,7,2,7,3,
                3,7,4,3,4,0,
                4,7,6,4,6,5,
                3,0,1,3,1,2
        };

        short indices2[] =  {0, 1, 2, 3,              //Front face
                5, 0, 3, 4,             //Right face
                5, 6, 7, 4,             //Back face
                5, 6, 1, 0,             //Upper face
                1, 6, 7, 2,              //Left face
                7, 4, 3, 2,             //Bottom face
        };

        float colors[] = {
                1.0f, 1.0f, 1.0f, 1.0f,
                0.0f, 0.5f, 0.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                0.0f, 0.5f, 0.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                0.0f, 0.5f, 0.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                0.0f, 0.5f, 0.0f, 1.0f

        };

        setIndices(indices);
        setVertices(vertices);
        setColors(colors);
    }


}
