package com.hqd.app.barplot.crom;

import android.database.Cursor;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by tomek on 4/19/14.
 */
public class CroBulk<T> {

    private Cursor cursor;

    public CroBulk(Cursor cursor) {
        this.cursor = cursor;
    }

    public List<T> convert(Class<T> clazz) {
        List<T> rows = Lists.newArrayList();
        Cro<T> cro = new Cro<T>(clazz);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            rows.add(cro.populate(cursor));
            cursor.moveToNext();
        }
        cursor.close();

        return rows;
    }

}
