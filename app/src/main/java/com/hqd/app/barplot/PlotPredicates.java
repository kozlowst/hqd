package com.hqd.app.barplot;

import com.google.common.base.Predicate;

/**
 * Created by tomek on 4/11/14.
 */
public class PlotPredicates {

    private static class LessThenPredicate implements Predicate<Integer> {

        private int value;

        private LessThenPredicate(int val) {
            value = val;
        }

        @Override
        public boolean apply(Integer input) {
            return input.compareTo(value) > 0;
        }

        public static LessThenPredicate lessThen(int value) {
            return new LessThenPredicate(value);
        }
    }

}
