package com.hqd.app.barplot;

import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.google.common.collect.Lists;
import com.hqd.app.rss.InfoData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by tomek on 4/10/14.
 * Class converts cursor data to data format acceptable by AndroidPlot
 */
public class CursorDataConverter {

    public static PlotData createPlotData(Cursor cursor, int colIndex) {

        Map<String, Integer> map = new HashMap<String, Integer>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            String key = cursor.getString(colIndex);
            int oldVal = 0;
            if (map.containsKey(key)) {
                oldVal = map.get(key);
            }
            map.put(key, oldVal + 1);
            cursor.moveToNext();
        }
        // TODO: find better way to add offset, for now this silly solution is ok
        map.put("", 0);
        cursor.close();

        String[] keys = new String[map.size()];
        map.keySet().toArray(keys);
        return new PlotData(Arrays.asList(keys), new ArrayList<Integer>(map.values()));
    }

    public List<Employee> migrate(Cursor cursor) {
        List<Employee> list = Lists.newArrayList();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {


            cursor.moveToNext();
        }
        return list;
    }

}
