package com.hqd.app.barplot;

import java.util.List;

/**
 * Created by tomek on 4/11/14.
 */
public class PlotData {
    private List<String> xData;
    private List<? extends Number> yData;
    private String label;

    public PlotData(List<String> x, List<? extends Number> y) {
        xData = x;
        yData = y;
    }

    public PlotData(List<String> x, List<? extends Number> y, String label) {
        this(x, y);
        this.label = label;
    }

    public List<? extends Number> getY() {
        return yData;
    }

    public List<String> getX() {
        return xData;
    }

}
