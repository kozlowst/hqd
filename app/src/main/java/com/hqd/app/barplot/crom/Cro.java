package com.hqd.app.barplot.crom;

import android.database.Cursor;

import com.hqd.app.barplot.crom.annotation.Column;
import com.hqd.app.barplot.crom.annotation.Table;

import java.lang.reflect.Field;

/**
 * Created by tomek on 4/19/14.
 *
 * Cursor Representation Object
 */
public class Cro<T> {

    private Class<T> clazz;

    public Cro(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T populate(Cursor cursor) {
        T obj = null;
        try {
            obj = clazz.newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String columnName = obtainColumnName(column, field.getName());
                        int type = column.type();
                        field.setAccessible(true);
                        field.set(obj, assignValue(type,cursor,columnName));
                    }
                }
            }
        } catch (InstantiationException e) {
            System.out.println("InstantiationException" + e.getMessage());
        } catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException" + e.getMessage());
        } catch (CroException e) {
            System.out.println("CroEx: " + e.getMessage());
        }


        return obj;
    }

    private Object assignValue(int type, Cursor cursor, String name) throws CroException {
        int index = cursor.getColumnIndex(name);
        if (index == -1) {
            throw new CroException("Column not exists.");
        }
        switch (type) {
            case Types.COLUMN_TYPE_INTEGER:
                int value = cursor.getInt(index);
                return new Integer(value);
            case Types.COLUMN_TYPE_STRING:
                return new String(cursor.getString(index));
        }
        return null;
    }

    private String obtainColumnName(Column column, String name) {
        if (column.name().isEmpty()) {
            return name;
        }
        return column.name();
    }

}
