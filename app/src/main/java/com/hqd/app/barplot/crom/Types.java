package com.hqd.app.barplot.crom;

/**
 * Created by tomek on 4/19/14.
 */

/**
 * int 	FIELD_TYPE_BLOB 	Value returned by getType(int) if the specified column type is blob
 int 	FIELD_TYPE_FLOAT 	Value returned by getType(int) if the specified column type is float
 int 	FIELD_TYPE_INTEGER 	Value returned by getType(int) if the specified column type is integer
 int 	FIELD_TYPE_NULL 	Value returned by getType(int) if the specified column is null
 int 	FIELD_TYPE_STRING 	Value returned by getType(int) if the specified column type is string */
public interface Types {
    static final int COLUMN_TYPE_BLOB = 0;
    static final int COLUMN_TYPE_FLOAT = 1;
    static final int COLUMN_TYPE_INTEGER = 2;
    static final int COLUMN_TYPE_NULL = 3;
    static final int COLUMN_TYPE_STRING = 4;
}
