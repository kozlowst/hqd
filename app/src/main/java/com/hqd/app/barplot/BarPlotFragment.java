package com.hqd.app.barplot;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;

import com.google.common.collect.Lists;
import com.hqd.app.R;
import com.hqd.app.providers.MyContract;

import java.util.List;

/**
 * Created by tomek on 4/17/14.
 */
public class BarPlotFragment extends ListFragment implements LoaderManager.LoaderCallbacks<PlotData> {
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG", "BarPlotFragment");
        List<PlotData> plotDatas = Lists.newArrayList(generate());
        setListAdapter(new PlotAdapter(getActivity(), R.layout.plot_data_layout, plotDatas));
        getLoaderManager().initLoader(1234, null, this).forceLoad();
    }

    private PlotData generate() {
        List<String> names = Lists.newArrayList(new String[]{""});
        List<Integer> occur = Lists.newArrayList(new Integer[]{0});
        return new PlotData(names, occur);
    }

    @Override
    public Loader<PlotData> onCreateLoader(int id, Bundle args) {
        Log.d("TAG", "Loader plot created");
        return new TaskLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<PlotData> loader, PlotData data) {
        Log.d("TAG", "Loader plot finished " + data.getY().toString() + ", " + data.getX().toString());
        setListAdapter(new PlotAdapter(getActivity(), R.layout.plot_data_layout, Lists.newArrayList(data)));
    }

    @Override
    public void onLoaderReset(Loader<PlotData> loader) {

    }

    public static class TaskLoader extends AsyncTaskLoader<PlotData> {

        public TaskLoader(Context context) {
            super(context);
        }

        @Override
        public PlotData loadInBackground() {
            Log.d("TAG", "Loader - start");
            Cursor cursor = getContext().getContentResolver().query(MyContract.Employees.CONTENT_URI,
                    MyContract.Employees.PROJECTION_ALL,
                    null,
                    null,
                    MyContract.Employees.SORT_ORDER_DEFAULT );
            Log.d("TAG", "Task query size: " + cursor.getCount());
            return CursorDataConverter.createPlotData(cursor, 1);
        }
    }
}
