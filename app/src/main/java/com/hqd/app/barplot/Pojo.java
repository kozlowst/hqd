package com.hqd.app.barplot;

import android.database.Cursor;

/**
 * Created by tomek on 4/18/14.
 */
public interface Pojo {
    void populate(Cursor cursor);
}
