package com.hqd.app.barplot;

import com.hqd.app.barplot.crom.annotation.Column;
import com.hqd.app.barplot.crom.annotation.Table;
import com.hqd.app.barplot.crom.Types;

/**
 * Created by tomek on 4/18/14.
 */
@Table
public class Employee {
    @Column(type = Types.COLUMN_TYPE_STRING)
    private String name;
    @Column(name="salaries", type = Types.COLUMN_TYPE_INTEGER)
    private Integer salary;

    public Employee() {

    }

    public Employee(String name, Integer salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public Integer getSalary() {
        return salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (name != null ? !name.equals(employee.name) : employee.name != null) return false;
        if (salary != null ? !salary.equals(employee.salary) : employee.salary != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (salary != null ? salary.hashCode() : 0);
        return result;
    }
}
