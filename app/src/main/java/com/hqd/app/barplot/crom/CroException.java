package com.hqd.app.barplot.crom;

/**
 * Created by tomek on 4/19/14.
 */
public class CroException extends Exception {
    public CroException(String detailMessage) {
        super(detailMessage);
    }
}
