package com.hqd.app.barplot;

import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepFormatter;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;
import com.hqd.app.R;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by tomek on 4/16/14.
 */
public class PlotAdapter extends ArrayAdapter<PlotData> {

    private Context context;
    private int resource;
    private List<PlotData> plotDataList;

    public PlotAdapter(Context context, int resource, List<PlotData> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.plotDataList = objects;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public PlotData getItem(int position) {
        return plotDataList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("TAG", "getView: pos=" + position + ", view= " + convertView);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(resource, parent, false);
        XYPlot mySimpleXYPlot = (XYPlot) convertView.findViewById(R.id.stepChartExamplePlot);

        final PlotData data = plotDataList.get(position);

        // y-vals to plot:
        //Number[] series1Numbers = {1, 2, 3, 4, 2, 3, 4, 2, 2, 2, 3, 4, 2, 3, 2, 2};
        //Number[] series1Numbers = {0, 3, 3, 6};
        // create our series from our array of nums:
        XYSeries series2 = new SimpleXYSeries(
                data.getY(),
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Names stat");

        mySimpleXYPlot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);
        mySimpleXYPlot.getGraphWidget().getDomainGridLinePaint().setColor(Color.BLACK);
        mySimpleXYPlot.getGraphWidget().getDomainGridLinePaint().setPathEffect(new DashPathEffect(new float[]{1, 1}, 1));
        mySimpleXYPlot.getGraphWidget().getRangeGridLinePaint().setColor(Color.BLACK);
        mySimpleXYPlot.getGraphWidget().getRangeGridLinePaint().setPathEffect(new DashPathEffect(new float[]{1, 1}, 1));
        mySimpleXYPlot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        mySimpleXYPlot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);
        mySimpleXYPlot.getGraphWidget().setMarginRight(5);

        BarFormatter barFormatter = new BarFormatter(Color.rgb(0, 100, 0), Color.rgb(100, 200, 0));

        // setup our line fill paint to be a slightly transparent gradient:
        Paint lineFill = new Paint();
        lineFill.setAlpha(200);
        lineFill.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.BLUE, Shader.TileMode.MIRROR));

        StepFormatter stepFormatter  = new StepFormatter(Color.rgb(0, 0,0), Color.BLUE);
        stepFormatter.getLinePaint().setStrokeWidth(1);

        stepFormatter.getLinePaint().setAntiAlias(false);
        stepFormatter.setFillPaint(lineFill);
        mySimpleXYPlot.addSeries(series2, barFormatter);

        // adjust the domain/range ticks to make more sense; label per tick for range and label per 5 ticks domain:
        mySimpleXYPlot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
        mySimpleXYPlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
        mySimpleXYPlot.setTicksPerRangeLabel(1);
        mySimpleXYPlot.setTicksPerDomainLabel(1);

        mySimpleXYPlot.setDomainValueFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                Number num = (Number) obj;
                toAppendTo.append(data.getX().get(num.intValue()));
                return toAppendTo;
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });

        // create a custom formatter to draw our state names as range tick labels:
        mySimpleXYPlot.setRangeValueFormat(new DecimalFormat("0"));

        return convertView;
    }
}
